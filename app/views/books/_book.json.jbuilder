json.extract! book, :id, :book_name, :book_author, :widrawal_status, :allowed_role_id, :created_at, :updated_at
json.url book_url(book, format: :json)