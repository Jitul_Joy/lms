class Person < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
    validates_confirmation_of :password
    has_many :book

    validates_presence_of :first_name, :email
    before_save :assign_role

#Check this definition it may cause issues
    def assign_role
      self.role_id = 3 if self.role_id.nil?
    end

#define roles here
    def librarian?
      self.role_id == 1
    end
    def prime_user?
      self.role.id == 2
    end
    def user?
      self.role.id == 3
    end
end
