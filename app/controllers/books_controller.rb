class BooksController < ApplicationController
  load_and_authorize_resource
  before_filter :authenticate_person!
  #before_action :set_book, only: [:show, :edit, :update, :destroy]

  # GET /books
  # GET /books.json
  def index
	if params[:search]
        @books = Book.where("book_name like ? and person_id is null", "#{params[:search]}%")
	else
      @books = Book.where("person_id is null and allowed_role_id >= ?", current_person.id)
		#@books = Book.where("allowed_role_id == ? ", "123")# In place of 123 we need to add the value of role_id to get the role based books
	end
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  # GET /books/new
  def new
    #@book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  # GET /books/1
  # GET /books/1.json
  def submit
    currentUser = current_person.id
    temp = params[:book_name]
      temp.each do |bookid|
          logger.info "***************#{bookid}"
          fetchedBook = Book.find(bookid) 
          fetchedBook.person_id = currentUser
          fetchedBook.save
      end
     redirect_to :action=>"index" 
    logger.info "Unnati borrows book #{params[:book_name]}"
  end

  # GET /books/1
  # GET /books/1.json
  def release
      logger.info "************** #{params[:book_id]}"
    @book = Book.find(params[:book_id])
@book.person_id = '';
@book.save
redirect_to :action=>"issuedbook"
  end

def issuedbook
	currentUser = current_person.id
	if Person.find(currentUser).librarian?
		@books = Book.where("person_id is not null")
	else
		@books = Book.where("person_id = ?", currentUser)
	end
end

  # POST /books
  # POST /books.json
  def create
    #@book.person_id = current_person.id
    #@book = Book.new(book_params)
    @book.widrawal_status = "false"

    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    #def set_book
    #  @book = Book.find(params[:id])
    #end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:book_name, :book_author, :widrawal_status, :allowed_role_id)
    end
end
