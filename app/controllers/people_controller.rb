class PeopleController < ApplicationController
  load_and_authorize_resource
  before_filter :authenticate_person!
  #before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = Person.all
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @joined_on = @person.created_at.to_formatted_s(:short)
    if @person.current_sign_in_at
      @last_login = @person.current_sign_in_at.to_formatted_s(:short)
    else
      @last_login = "Never"
    end
  end

  # GET /people/new
  def new
    #@person = Person.new
  end

  # GET /people/1/edit
  def edit
    authorize! :edit, @book
  end

  # POST /people
  # POST /people.json
  def create
    #@person = Person.new(person_params)
    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    if person_params[:password].blank?
      person_params.delete(:password)
      person_params.delete(:password_confirmation)
    end

    successfully_updated = if needs_password?(@person, person_params)
                             @person.update(person_params)
                           else
                             @person.update_without_password(person_params)
                           end

    respond_to do |format|
      if successfully_updated
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    #def set_person
    #  @person = Person.find(params[:id])
    #end
    protected
    def needs_password?(person, params)
      params[:password].present?
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:first_name, :last_name, :gender, :dob, :phone_number, :email, :password, :password_confirmation, :role_id)
    end
end
