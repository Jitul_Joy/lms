class ApplicationController < ActionController::Base
  before_filter :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

  def current_user
    @current_user ||= current_person
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Access denied!"
    redirect_to root_url
  end  

  protected
  def configure_permitted_parameters
	devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name,:gender, :dob, :phone_number,:role_id])
	devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name,:gender, :dob, :phone_number,:role_id])
  end

end
