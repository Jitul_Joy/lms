class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.date :dob
      t.string :phone_number
      t.string :email
      t.string :password
      t.integer :role_id

      t.timestamps
    end
  end
end
