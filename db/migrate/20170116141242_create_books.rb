class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :book_name
      t.string :book_author
      t.boolean :widrawal_status
      t.integer :allowed_role_id
        t.references :person, foreign_key: true, :null => true
      t.timestamps
    end
  end
end
