# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#Role.create!([{role_name: 'Librarian'},{role_name: 'User'},{role_name: 'Prime User'}])
rLib = Role.create!({role_name: 'Librarian'})
rP_User = Role.create!({role_name: 'Prime User'})
rUser = Role.create!({role_name: 'User'})
p1 = Person.create_with(password: '123456', role_id: 1, first_name: 'Mandar', last_name: 'Joshi', dob: '1991-01-01', phone_number: '12365478', gender: 'Male').find_or_create_by(email: 'mandar@test.com')
p2 = Person.create_with(password: '123456', role_id: 1, first_name: 'Solomon', last_name: 'Ronald', dob: '1993-05-31', phone_number: '66662225', gender: 'Male').find_or_create_by(email: 'solomon@test.com')
p3 = Person.create_with(password: '123456', role_id: 1, first_name: 'Unnati', last_name: 'Khanorkar', dob: '1990-05-21', phone_number: '965423125', gender: 'Female').find_or_create_by(email: 'unnati@test.com')
p4 = Person.create_with(password: '123456', role_id: 2, first_name: 'Jitul', last_name: 'Joy', dob: '1992-06-12', phone_number: '12456325', gender: 'Male').find_or_create_by(email: 'jitul@test.com')
p5 = Person.create_with(password: '123456', role_id: 2, first_name: 'Snehal', last_name: 'Patil', dob: '1992-04-19', phone_number: '88889955', gender: 'Male').find_or_create_by(email: 'snehal@test.com')
p6 = Person.create_with(password: '123456', role_id: 3, first_name: 'Rahul', last_name: 'Chaturvedi', dob: '1991-01-24', phone_number: '56984213', gender: 'Male').find_or_create_by(email: 'rahul@test.com')
p7 = Person.create_with(password: '123456', role_id: 2, first_name: 'Mehul', last_name: 'Soni', dob: '1989-11-11', phone_number: '4455663322', gender: 'Male').find_or_create_by(email: 'mehul@test.com')
p8 = Person.create_with(password: '123456', role_id: 3, first_name: 'Roshan', last_name: 'Deshmukh', dob: '1993-12-21', phone_number: '246215522', gender: 'Male').find_or_create_by(email: 'roshan@test.com')
p9 = Person.create_with(password: '123456', role_id: 3, first_name: 'Navneet', last_name: 'Bhatia', dob: '1994-09-03', phone_number: '254789236', gender: 'Female').find_or_create_by(email: 'navi@test.com')
p10 = Person.create_with(password: '123456', role_id: 2, first_name: 'Suraj', last_name: 'Mahalle', dob: '1989-08-23', phone_number: '778955466', gender: 'Male').find_or_create_by(email: 'suraj@test.com')

