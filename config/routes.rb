Rails.application.routes.draw do

  post '/books/submit', to: 'books#submit'
  get '/books/issuedbook', to: 'books#issuedbook'
  get '/books/release', to: 'books#release'
  get '/people/new', to: 'people#new'
  devise_for :people
  scope "/librarian" do
    resources :people
  end
  resources :books
  resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  authenticated :person do
      root :to => 'books#index', as: :authenticated_root
  end
  root to: "books#index"
end
